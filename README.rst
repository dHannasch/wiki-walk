========
Overview
========

An example package. Generated with cookiecutter-pylibrary.

Installation
============

::

    pip install wiki-walk

You can also install the in-development version with::

    pip install https://gitlab.com/dHannasch/wiki-walk/-/archive/master/wiki-walk-master.zip


Documentation
=============


http://dHannasch.gitlab.io/wiki-walk


Development
===========

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox

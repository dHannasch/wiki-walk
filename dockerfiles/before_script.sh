right_after_pull_docker_image=$(date +%s)
cat /etc/os-release || echo "cat /etc/os-release failed."
lsb_release -a || echo "lsb_release -a failed."
hostnamectl || echo "hostnamectl failed."
uname -r || echo "uname -r failed."
uname --all || echo "uname --all failed."
echo $(whoami)
echo $USER
if ! [ -z ${GITLAB_CI+ABC} ]; then
set -o errexit
fi
if [ -z ${CI_SERVER_HOST+ABC} ]; then
CI_SERVER_HOST=gitlab.com
fi
if [ -z ${ETC_ENVIRONMENT_LOCATION+ABC} ]; then
ETC_ENVIRONMENT_LOCATION=https://$CI_SERVER_HOST/shell-bootstrap-scripts/network-settings/-/raw/master/set_variables_in_CI.sh
echo "ETC_ENVIRONMENT_LOCATION was unset, so trying $ETC_ENVIRONMENT_LOCATION."
fi
if [ -z ${ETC_ENVIRONMENT_LOCATION} ]; then echo "ETC_ENVIRONMENT_LOCATION is set to the empty string; I hope you know why, because I certainly do not."; fi
if [ -z ${FIX_ALL_GOTCHAS_SCRIPT_LOCATION+ABC} ]; then
FIX_ALL_GOTCHAS_SCRIPT_LOCATION=https://$CI_SERVER_HOST/shell-bootstrap-scripts/shell-bootstrap-scripts/-/raw/master/fix_all_gotchas.sh
fi
echo "ETC_ENVIRONMENT_LOCATION = $ETC_ENVIRONMENT_LOCATION"
mkdir -p ~/.ssh
echo "PasswordAuthentication=no" >> ~/.ssh/config
ls environment.sh || (wget --proxy off $ETC_ENVIRONMENT_LOCATION --output-document environment.sh --no-clobber && echo "Successfully downloaded $ETC_ENVIRONMENT_LOCATION using wget!") || (wget --no-proxy $ETC_ENVIRONMENT_LOCATION --output-document environment.sh && echo "Successfully downloaded $ETC_ENVIRONMENT_LOCATION using wget!") || (wget --no-proxy --no-check-certificate $ETC_ENVIRONMENT_LOCATION --output-document environment.sh && echo "Successfully downloaded $ETC_ENVIRONMENT_LOCATION using wget!") || (wget --help && wget $ETC_ENVIRONMENT_LOCATION --output-document environment.sh) || curl --verbose --location $ETC_ENVIRONMENT_LOCATION --output environment.sh || (echo $SSH_PRIVATE_DEPLOY_KEY > SSH.PRIVATE.KEY && scp -i SSH.PRIVATE.KEY $ETC_ENVIRONMENT_LOCATION environment.sh && rm SSH.PRIVATE.KEY)
cat environment.sh
SAVED_PATH=$PATH
set -o allexport
. ./environment.sh
set +o allexport
PATH=$SAVED_PATH
if hostname -i; then
no_proxy="$(hostname -i),$no_proxy"
fi
if [ -z ${SSH_PRIVATE_DEPLOY_KEY+ABC} ]; then echo "SSH_PRIVATE_DEPLOY_KEY is unset, so assuming you do not need SSH set up."; else
if [ ${#SSH_PRIVATE_DEPLOY_KEY} -le 5 ]; then echo "SSH_PRIVATE_DEPLOY_KEY looks far too short, something is wrong"; fi
if command -v ssh; then echo "Something that looks like ssh is already installed."; else
right_before_install_ssh=$(date +%s)
apk add openssh-client || (sed -i -e 's/https/http/' /etc/apk/repositories && apk add openssh-client) || apt-get install --assume-yes openssh-client || (apt-get update && apt-get install --assume-yes openssh-client)  || echo "Failed to install openssh-client; proceeding anyway to see if this image has its own SSH."
echo "adding openssh-client took $(( $(date +%s) - right_before_install_ssh)) seconds"
fi
eval $(ssh-agent -s)
if command -v ssh-add; then
echo "$SSH_PRIVATE_DEPLOY_KEY" | tr -d '\r' | ssh-add -
echo "Added the private SSH deploy key with public fingerprint $(ssh-add -l)"
echo "WARNING! If you use this script to build a Docker image (rather than just run tests), make sure to delete the deploy key with ssh-add -D after installing the relevant repos."
else
echo "It appears that this system does not have ssh-add, and we already failed to install openssh-client. You specified a SSH_PRIVATE_DEPLOY_KEY, so...we're just hoping you don't actually need that on this image/job."
fi
mkdir --parents ~/.ssh
echo "# github.com:22 SSH-2.0-babeld-f345ed5d\n" >> ~/.ssh/known_hosts
echo "github.com ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==\n" >> ~/.ssh/known_hosts
echo "# gitlab.com:22 SSH-2.0-OpenSSH_7.2p2 Ubuntu-4ubuntu2.8\n" >> ~/.ssh/known_hosts
echo "gitlab.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9\n" >> ~/.ssh/known_hosts
fi
if [ -z ${SERVERS_TO_WHITELIST_FOR_SSH+ABC} ] || [ -z ${SSH_PRIVATE_DEPLOY_KEY+ABC} ]; then echo "SERVERS_TO_WHITELIST_FOR_SSH and SSH_PRIVATE_DEPLOY_KEY are not both set, so assuming you do not need any servers whitelisted for SSH."; else
echo "SERVERS_TO_WHITELIST_FOR_SSH = $SERVERS_TO_WHITELIST_FOR_SSH"
mkdir --parents ~/.ssh
if command -v ssh-keyscan; then
ssh-keyscan -t rsa $SERVERS_TO_WHITELIST_FOR_SSH >> ~/.ssh/known_hosts
else
echo "SERVERS_TO_WHITELIST_FOR_SSH, but ssh-keyscan is not available and we already failed to install openssh-client, so...we're just hoping you don't actually need that on this image/job."
fi
fi
echo "python --version"
(python --version && python -c "import sys; print(sys.version_info.major)") || echo "python is not found by that name."
if command -v python3 && (! command -v python || [ $(python -c "import sys; print(sys.version_info.major)") -lt 3 ]); then
echo "python3 --version"
python3 --version || echo "python3 is not found by that name."
python() {
python3 "$@"
}
export -f python
echo "python --version"
(python --version && python -c "import sys; print(sys.version_info.major)") || echo "python is not found by that name."
fi
if command -v conda; then echo "command finds conda"; else echo "command does not find conda"; fi
if ! command -v conda && [ -d /opt/conda ]; then
CONDA_DIR=/opt/conda
PATH=$CONDA_DIR/bin:$PATH
if ! command -v conda; then false; fi
fi
if command -v conda; then
if [ "$CONDA_DEFAULT_ENV" = "test-env" ]; then echo "This image already has test-env activated."; else
conda env list
if conda env list | grep test-env; then
if source activate test-env; then true; else echo "No conda env named test-env was found, so not activating any particular env."; fi
else echo "We did not see test-env in conda env list. You would think we could simply try to activate test-env and pass if that returned false, but strangely, on some systems activate test-env kills the whole script when test-env does not exist, even though it's a condition in an if."
fi
fi
else
echo "conda was not found on this container"
fi
if [ -z ${PROXY_CA_PEM+ABC} ]; then echo "PROXY_CA_PEM is unset, so assuming you do not need a merged CA certificate set up."; else
right_before_pull_cert=$(date +%s)
if [ ${#PROXY_CA_PEM} -ge 1024 ]; then
echo "The PROXY_CA_PEM filename looks far too long, did you set it as a Variable instead of a File?"
echo "$PROXY_CA_PEM" > tmp-proxy-ca.pem
PROXY_CA_PEM=tmp-proxy-ca.pem ; fi
echo "PROXY_CA_PEM found at $(ls $PROXY_CA_PEM)"
if [ $(id -u) -eq 0 ]; then
if command -v apk; then
apk add --no-cache ca-certificates --repository http://dl-cdn.alpinelinux.org/alpine/edge/main/ --allow-untrusted
fi
if command -v update-ca-certificates; then
ls /usr/local/share/ca-certificates/
cp -f $PROXY_CA_PEM /usr/local/share/ca-certificates/$PROXY_CA_PEM.crt || ls /usr/local/share/ca-certificates/$PROXY_CA_PEM.crt
update-ca-certificates
else
if command -v update-ca-trust; then
ls /etc/pki/ca-trust/source/anchors/
cp $PROXY_CA_PEM /etc/pki/ca-trust/source/anchors/
update-ca-trust
else
if ls /etc/ssl/certs/ca-certificates.crt; then
PROXY_CA_PEM_NUM_LINES=$(wc -l < $PROXY_CA_PEM)
if [ $PROXY_CA_PEM_NUM_LINES -lt 8 ] || [ $PROXY_CA_PEM_NUM_LINES -gt 64 ]; then
echo "PROXY_CA_PEM_NUM_LINES is $PROXY_CA_PEM_NUM_LINES, something is terribly wrong."
false; fi
if [ "$(tail -n $PROXY_CA_PEM_NUM_LINES /etc/ssl/certs/ca-certificates.crt)" != "$(cat $PROXY_CA_PEM)" ]; then
cat $PROXY_CA_PEM >> /etc/ssl/certs/ca-certificates.crt
if [ "$(tail -n $PROXY_CA_PEM_NUM_LINES /etc/ssl/certs/ca-certificates.crt)" != "$(cat $PROXY_CA_PEM)" ]; then false; fi
fi
fi
fi
fi
fi
if command -v wget; then
right_before_set_up_wget=$(date +%s)
echo "ca_certificate=${PWD%/}/$PROXY_CA_PEM" >> $HOME/.wgetrc
cat $HOME/.wgetrc
echo "Setting up wget took $(( $(date +%s) - right_before_set_up_wget)) seconds"
fi
if command -v curl; then
right_before_set_up_curl=$(date +%s)
ls /etc/ssl/certs/
cat $PROXY_CA_PEM > bundled.pem
if ls /etc/ssl/certs/ca-certificates.crt; then cat /etc/ssl/certs/ca-certificates.crt >> bundled.pem; fi
if ls /etc/ssl/certs/ca-bundle.crt; then cat /etc/ssl/certs/ca-bundle.crt >> bundled.pem; fi
if ls /etc/ssl/certs/ca-bundle.trust.crt; then cat /etc/ssl/certs/ca-bundle.trust.crt >> bundled.pem; fi
echo "cacert=${PWD%/}/bundled.pem" >> $HOME/.curlrc
cat $HOME/.curlrc
echo "Setting up curl took $(( $(date +%s) - right_before_set_up_curl)) seconds"
fi
if command -v conda; then
conda config --set proxy_servers.http $http_proxy
conda config --set proxy_servers.https $https_proxy
conda config --set ssl_verify $PROXY_CA_PEM
conda config --set remote_read_timeout_secs 60
fi
right_before_install_nss=$(date +%s)
if [ -d $HOME/.pki/nssdb ]; then ls -l $HOME/.pki/nssdb/; else
echo "$HOME/.pki/nssdb not found; looking to create it."
if command -v apk && [ $(id -u) -eq 0 ]; then
if [ -d pki/nssdb/ ]; then
echo "Found pki/nssdb/ so copying that."
else
echo "No NSS DB found, but $PROXY_CA_PEM found, so creating an NSS DB."
apk add nss-tools || (sed -i -e 's/https/http/' /etc/apk/repositories && apk add nss-tools)
mkdir --parents pki/nssdb/
certutil -N -d pki/nssdb/ --empty-password
certutil -d sql:pki/nssdb/ -A -t "C,," -n proxycertasCA -i $PROXY_CA_PEM
apk del nss-tools
fi
ls -l pki/nssdb/
mkdir --parents $HOME/.pki/nssdb/
cp -r pki/nssdb/ $HOME/.pki/
echo $HOME/.pki/nssdb
ls -l $HOME/.pki/nssdb
fi
fi
echo "adding cert to nss db took $(( $(date +%s) - right_before_install_nss)) seconds"
if command -v python && [ $(python -c "import sys; print(sys.version_info.major)") -ge 3 ] && python -m pip; then
python --version
python -m pip --version || echo "The executable called python does not have pip."
python -c "import contextlib; contextManager = contextlib.suppress(AttributeError); contextManager.__enter__(); import pip._vendor.requests; contextManager.__exit__(None,None,None); from pip._vendor.requests.certs import where; print(where())"
cat $(python -c "import contextlib; contextManager = contextlib.suppress(AttributeError); contextManager.__enter__(); import pip._vendor.requests; contextManager.__exit__(None,None,None); from pip._vendor.requests.certs import where; print(where())") ${PROXY_CA_PEM} > bundled.pem
ls bundled.pem
export REQUESTS_CA_BUNDLE="${PWD%/}/bundled.pem"
export GIT_SSL_CAINFO="${PWD%/}/bundled.pem"
echo "REQUESTS_CA_BUNDLE found at $(ls $REQUESTS_CA_BUNDLE)"
echo "Merging the certificate bundle took $(( $(date +%s) - right_before_pull_cert)) seconds total"
fi
fi
if [ -z ${PYPI_URL+ABC} ]; then echo "PYPI_URL is unset."; else
echo "PYPI_URL is set to $PYPI_URL"
if [ "${PYPI_URL%/}" == "${PYPI_URL}" ]; then
echo "PYPI_URL has no trailing /. If you try to twine upload without the trailing /, you'll get back an HTTPError 400 Bad Request Repository path must have another '/' after initial '/'."
fi
PYPI_URL=${PYPI_URL%/}/
PIP_INDEX_URL="${PYPI_URL}simple"
echo "PIP_INDEX_URL $PIP_INDEX_URL"
PIP_EXTRA_INDEX_URL=https://pypi.org/simple
echo "PIP_EXTRA_INDEX_URL $PIP_EXTRA_INDEX_URL"
fi
if command -v conda; then
echo "stat -c '%u' $(which conda); id -u"
stat -c '%u' $(which conda)
id -u
if [ $(stat -c '%u' $(which conda)) -eq $(id -u) ]; then
if conda list --name base | grep conda-build; then
conda update --yes --name base conda-build
else echo "We did not see conda-build in conda list. You would think we could simply try conda list conda-build since that's what it's for, but strangely, on some systems conda list conda-build kills the whole script when conda-build is not installed, even though it's a condition in an if."
fi
else
echo "This user does not own $(which conda), so we cannot update the conda base environment."
fi
fi
if command -v jupyter; then
python -m pip install ipykernel
python -m ipykernel install || echo "Failed to install the default python3 to ipykernel. You might still be able to ipykernel install your preferred env."
fi
if [ -z ${DOCS_REQUIRE_PACKAGE+ABC} ]; then
echo "DOCS_REQUIRE_PACKAGE is not set, so we will leave it to the test job to install the package."
else
if command -v python && [ $(python -c "import sys; print(sys.version_info.major)") -ge 3 ] && ls setup.py; then
python -m pip install --upgrade pip
right_before_pip_install=$(date +%s)
python -m pip install .
echo "Installing your package took $(( $(date +%s) - right_before_pip_install)) seconds total"
fi
fi
if command -v tox; then
python -m pip install --upgrade setuptools
fi
echo "before_script took $(( $(date +%s) - right_after_pull_docker_image)) seconds total"

import json
import os.path
import pathlib
import queue
import time
import urllib.parse

import selenium.webdriver


def create_driver():
  options = selenium.webdriver.ChromeOptions()
  options.add_argument('--incognito')  # for example
  # options.add_argument('--proxy-server=localhost:8000')
  options.binary_location = '/usr/bin/chromium-browser'
  assert os.path.exists(options.binary_location)
  # ./chromedriver must be separately downloaded: https://www.selenium.dev/documentation/getting_started/installing_browser_drivers/
  for loc in ('./chromedriver', '/tmp/chromedriver'):
    if os.path.exists(loc):
      chromedriver_location = loc
  return selenium.webdriver.Chrome(chromedriver_location, options=options)


class WikiWalk:
  @staticmethod
  def from_file(filepath: pathlib.Path):
    with open(filepath, 'r') as jsonFile:
      jsonDict = json.load(jsonFile)
    toVisit = queue.LifoQueue()
    for URL in jsonDict['toVisit']:
      toVisit.put(URL)
    for URL, entry in jsonDict['visited'].items():
      if not entry or not entry['complete']:
        toVisit.put(URL)
    return WikiWalk(toVisit, jsonDict['visited'], set(jsonDict['ignore']))

  @staticmethod
  def from_URL(URL: str):
    toVisit = queue.LifoQueue()
    toVisit.put(URL)
    return WikiWalk(toVisit, dict())

  def __init__(self, toVisit: queue.LifoQueue, visited: dict = dict(), ignore: set = set()):
    self.toVisit = toVisit
    self.visited = visited
    self.ignore = ignore
    self.ignore.add('data:,')

  def save_as_file(self, filepath: pathlib.Path):
    jsonDict = dict()
    jsonDict['toVisit'] = list()
    while not self.toVisit.empty():
      nextURL = self.toVisit.get()
      if nextURL not in self.visited:
        jsonDict['toVisit'].append(nextURL)
    jsonDict['toVisit'] = jsonDict['toVisit'][::-1]
    jsonDict['visited'] = self.visited
    jsonDict['ignore'] = list(self.ignore)
    with open(filepath, 'w') as jsonFile:
      json.dump(jsonDict, jsonFile)


def wait_for_URL_change(driver, ignore: set = set()):
  currentURL = driver.current_url
  while currentURL in driver.current_url or driver.current_url in currentURL or driver.current_url in ignore:
    time.sleep(1)
  return driver.current_url


def walk(filepath: pathlib.Path, startURL: str = None):
  if os.path.exists(filepath):
    record = WikiWalk.from_file(filepath)
    if startURL:
      record.toVisit.put(startURL)
      if startURL in record.visited and 'complete' in record.visited[startURL] and record.visited[startURL]['complete']:
        record.visited[startURL]['complete'] = False
        print(startURL, 'was previously marked as complete, erasing that mark to revisit.')
  else:
    if not startURL:
      raise ValueError("Either a starting file or a starting URL must be provided.")
    record = WikiWalk.from_URL(startURL)
  driver = create_driver()
  try:
    while not record.toVisit.empty():
      nextURL = record.toVisit.get()
      print('nextURL =', nextURL)
      if nextURL not in record.visited:
        record.visited[nextURL] = dict()
      elif 'complete' in record.visited[nextURL] and record.visited[nextURL]['complete']:
        continue
      driver.get(nextURL)
      clickedURL = wait_for_URL_change(driver, record.ignore)
      assert clickedURL != nextURL
      while 'localhost:12345' not in clickedURL:
        print('clicked', clickedURL)
        if clickedURL not in record.ignore:
          record.toVisit.put(clickedURL)
        if driver.current_url != clickedURL:
          raise ValueError(driver.current_url, clickedURL)
        assert nextURL not in driver.current_url
        driver.back()  # not done with this yet
        # driver.execute_script("window.history.go(-1)")
        for i in range(4):
          if nextURL in driver.current_url:
            break
          print('Trying again to go back from', driver.current_url, 'to', nextURL)
          if i % 2:
            driver.execute_script("window.history.go(-1)")
          else:
            driver.back()
          for i in range(4):
            if nextURL in driver.current_url:
              break
            print('Waiting for', driver.current_url, 'to go back to', nextURL)
            time.sleep(1)
        if nextURL not in driver.current_url:
          raise ValueError(driver.current_url, nextURL)
        clickedURL = wait_for_URL_change(driver)
      assert 'localhost:12345' in clickedURL
      record.visited[nextURL]['complete'] = True
  finally:
    record.save_as_file(filepath)
    driver.quit()


def walk_file_or_URL(file_or_URL: str):
  if os.path.exists(file_or_URL):
    return walk(file_or_URL)
  if not urllib.parse.urlparse(file_or_URL):
    raise ValueError(file_or_URL)
  return walk('WikiWalk.json', file_or_URL)

